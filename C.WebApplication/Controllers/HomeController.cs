﻿using C.WebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace C.WebApplication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Operation()
        {         
            return View();
        }
        /// <summary>
        /// Metodo que recibe el Formulario (modelo Vista ) define la conexion contra la logica del negocio.
        /// </summary>
        /// <param name="form">Modelo donde se define la colección de datos a evaluar.</param>
        /// <returns></returns>
        public ActionResult OperationService(OperationViewModel form)
        {
            try
            {
                WebServiceReference.Service1Client reference = new WebServiceReference.Service1Client();
                ViewBag.Result = reference.GetSum(form.input1, form.input2);

                //ViewBag.Message = 
            }
            catch (Exception)
            {

                ViewBag.Result = "Servicio no disponible";
            }
            return View("Result");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}