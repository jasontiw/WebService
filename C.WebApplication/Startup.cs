﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(C.WebApplication.Startup))]
namespace C.WebApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
