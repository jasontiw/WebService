﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace C.WebApplication.Models
{
    public class OperationViewModel
    {
        [Required]
        [DataType(DataType.Currency)]
        [Display(Name = "Campo 1")]      
        public int input1 { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        [Display(Name = "Campo 2")]
        public int input2 { get; set; }       
    }
}